fs = require 'fs'
path = require 'path'
csv = require 'csv'
trim = require 'trim'
_ = require 'lodash'
normalizer = require './libs/normalizer'
request = require 'request'
crypto = require 'crypto'
nimble = require 'nimble'
expander = require './libs/acronym-expander'
utils = require './libs/utils'
converter = require 'json-2-csv'

# if (process.env.COUNTRY is undefined)
#   console.log ''
#   process.exit 1

count = 0
noOfItems = 0
rawData = []
data = []

parser = csv.parse (err, data) ->

	count = 0
	noOfItems = data.length
	# noOfItems = 20
	rawData = data
	data = []

	# console.log '>> next item:', count
	parseRow rawData[count]

parseRow = (item) ->

	lce = {}

	lce.id = count + 1
	lce.dbapp_s = 'loce'

	# offender..
	nameSet = getOffenderName item[1]
	offender = normalizer.normalizePteLtd nameSet.name
	if nameSet.designation?
		designation = _.trim nameSet.designation
		designation = normalizer.normalizePteLtd designation

	# industry
	industrySet = getIndustrySet item[2]
	industryCode = industrySet.code
	industryName = industrySet.name

	# sentenceDate
	sentenceDate = removeGarbage(item[3]) if item[3] isnt ''

	date = new Date(sentenceDate)
	year = date.getFullYear()

	# noOfCharges..
	noOfCharges = parseInt item[4]

	# typeOfOffence..
	typeOfOffence = removeGarbage(item[5]) if item[5] isnt ''
	typeOfOffence = _.trim typeOfOffence

	# penalty..
	penalty = item[6] if item[6] isnt ''
	if (!isNaN(penalty)) then penalty = '$' + parseInt(penalty).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

	lce.title_t = offender
	lce.designation_t = designation
	lce.industryCode_i = industryCode
	lce.industryName_s = industryName
	if sentenceDate isnt undefined then lce.sentenceDate_tdt = sentenceDate
	if year isnt undefined then lce.year_i = year
	if noOfCharges isnt undefined then lce.noOfCharges_i = noOfCharges
	if typeOfOffence isnt undefined then lce.typeOfOffence_s = typeOfOffence
	if penalty isnt undefined then lce.penalty_s = penalty

	# lce.hash = crypto.createHash('md5').update(offender).digest('hex')

	data.push lce
	getNextItem()

getNextItem = ->
	if (count < noOfItems - 1)
		count++
		# console.log '>> next item:', count
		parseRow rawData[count]
	else
		# dedupeData()
		writeData data

getIndustrySet = (industry) ->
	# console.log industry
	set = {}
	re = /(\d{3})(\s*-\s*)(.*)/
	result = re.exec industry
	# console.log 'code:', result
	set.code = _.trim result[1]
	set.name = _.trim result[3]
	return set

getOffenderName = (name) ->
	# console.log name.split("\n")
	names = name.split("\n")
	nameSet = {}
	nameFirstLine = _.trim(names[0])
	if (nameFirstLine is '')
		names.splice 0, 1
		name = _.trim(names.join(" "))
		if name? and name isnt '' then nameSet.name = names.join(" ")
	else 
		nameSet.name = nameFirstLine
		if (names.length > 1)
			names.splice 0, 1
			des = _.trim(names.join(" "))
			if des? and des isnt '' then nameSet.designation = names.join(" ")
			
	return nameSet

	# if name.match /\n/
	# 	re = /(.*)\n(.*)/
	# 	result = re.exec name
	# 	# console.log result
	# 	# console.log _.trim(result[0])
	# 	return name
	# else
	# 	return name

dedupeData = ->
	cleanData = []

	# data = _.sortBy data, 'hash'
	data = _.groupBy data, 'hash'
	_.each data, (item) ->
		# console.log item.length
		if (item.length is 1)
			cleanData.push item[0]
		else
			provider = item[0]
			i = 1
			while i < item.length
				# provider.email_ss = _.union provider.email_ss, item[i].email_ss
				provider.sentenceDate_ss = _.union provider.sentenceDate_ss, item[i].sentenceDate_ss
				# provider.fullsentenceDate_ss = _.union provider.fullsentenceDate_ss, item[i].fullsentenceDate_ss
				# provider.sentenceDateFacet_ss = _.union provider.sentenceDateFacet_ss, item[i].sentenceDateFacet_ss
				provider.industry_ss = _.union(provider.industry_ss, item[i].industry_ss) # if item[i].industry_ss[0] isnt null
				# provider.role_ss = _.union(provider.role_ss, item[i].role_ss)
				i++
			if provider.industry_ss.length is 0 then delete provider.industry_ss

			cleanData.push provider

	writeData cleanData

removeGarbage = (str) ->
	if str is undefined then return
	cleaned = str.replace /Ê|�/gi, " "
	cleaned = cleaned.replace /\\r/, ""
	cleaned = cleaned.replace /\s+/g, " "
	cleaned = cleaned.replace /<\/strong>.*/g, ""
	cleaned = cleaned.replace /Website:.*/gi, ""
	return _.trim(cleaned)

writeData = (cleanData) ->
	
	obj =
		id: '__last_modified'
		lastupdated_s: utils.getShortDate()

	cleanData.push obj

	filename = 'list-of-convicted-employers.json'
	console.log '>> saving data to:', filename
	fs.writeFileSync 'output/' + filename, JSON.stringify(cleanData), { ecoding: 'utf-8' }

	console.log '** complete! ** num docs:', cleanData.length


nimble.parallel [
	(cb) ->
		normalizer.loadTerms './protwords.txt', ->
			cb()
	(cb) ->
		expander.loadTerms './terms.csv', ->
			cb()
], ->
	# console.log 'done..', expander.terms
	fs.createReadStream('csv/list-of-convicted-employers.csv').pipe parser

# normalizer.loadTerms './protwords.txt', ->
# 	fs.createReadStream('csv/lce-sample-raw-data.csv').pipe parser