fs = require 'fs'
path = require 'path'
csv = require 'csv'
trim = require 'trim'
_ = require 'lodash'
normalizer = require './libs/normalizer'
request = require 'request'
crypto = require 'crypto'
nimble = require 'nimble'
expander = require './libs/acronym-expander'
utils = require './libs/utils'
converter = require 'json-2-csv'

# if (process.env.COUNTRY is undefined)
#   console.log ''
#   process.exit 1

count = 0
noOfItems = 0
rawData = []
data = []

parser = csv.parse (err, data) ->

	count = 0
	noOfItems = data.length
	# noOfItems = 20
	rawData = data
	data = []

	# console.log '>> next item:', count
	parseRow rawData[count]

parseRow = (item) ->

	eti = {}

	eti.id = count + 1
	eti.dbapp_s = 'eti'

	# institution..
	institution = item[0] if item[0] isnt ''

	# region
	region = item[1] if item[1] isnt ''

	# country
	country = item[2] if item[2] isnt ''

	eti.title_t = institution
	if region isnt undefined then eti.region_s = region
	if country isnt undefined then eti.country_s = country

	# eti.hash = crypto.createHash('md5').update(institution).digest('hex')

	data.push eti
	getNextItem()

getNextItem = ->
	if (count < noOfItems - 1)
		count++
		# console.log '>> next item:', count
		parseRow rawData[count]
	else
		# dedupeData()
		writeData data

dedupeData = ->
	cleanData = []

	# data = _.sortBy data, 'hash'
	data = _.groupBy data, 'hash'
	_.each data, (item) ->
		# console.log item.length
		if (item.length is 1)
			cleanData.push item[0]
		else
			provider = item[0]
			i = 1
			while i < item.length
				# provider.email_ss = _.union provider.email_ss, item[i].email_ss
				provider.sentenceDate_ss = _.union provider.sentenceDate_ss, item[i].sentenceDate_ss
				# provider.fullsentenceDate_ss = _.union provider.fullsentenceDate_ss, item[i].fullsentenceDate_ss
				# provider.sentenceDateFacet_ss = _.union provider.sentenceDateFacet_ss, item[i].sentenceDateFacet_ss
				provider.industry_ss = _.union(provider.industry_ss, item[i].industry_ss) # if item[i].industry_ss[0] isnt null
				# provider.role_ss = _.union(provider.role_ss, item[i].role_ss)
				i++
			if provider.industry_ss.length is 0 then delete provider.industry_ss

			cleanData.push provider

	writeData cleanData

removeGarbage = (str) ->
	if str is undefined then return
	cleaned = str.replace /Ê|�/gi, " "
	cleaned = cleaned.replace /\\r/, ""
	cleaned = cleaned.replace /\s+/g, " "
	cleaned = cleaned.replace /<\/strong>.*/g, ""
	cleaned = cleaned.replace /Website:.*/gi, ""
	return _.trim(cleaned)

writeData = (cleanData) ->
	
	obj =
		id: '__last_modified'
		lastupdated_s: utils.getShortDate()

	cleanData.push obj

	filename = 'ep-training-institution.json'
	console.log '>> saving data to:', filename
	fs.writeFileSync 'output/' + filename, JSON.stringify(cleanData), { ecoding: 'utf-8' }

	console.log '** complete! ** num docs:', cleanData.length


nimble.parallel [
	(cb) ->
		normalizer.loadTerms './protwords.txt', ->
			cb()
	(cb) ->
		expander.loadTerms './terms.csv', ->
			cb()
], ->
	# console.log 'done..', expander.terms
	fs.createReadStream('csv/ep-training-institution.csv').pipe parser

# normalizer.loadTerms './protwords.txt', ->
# 	fs.createReadStream('csv/eti-sample-raw-data.csv').pipe parser