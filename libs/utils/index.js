module.exports = {

  getFullDate: function () {
    var d = new Date(),
        date = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate(),
        month = d.getMonth() + 1,
        hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();

    month = (month < 10) ? '0' + month : month;
    hours = (hours < 10) ? '0' + hours : hours;
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds : seconds;

    return d.getFullYear() + '-' + month + '-' + date + 'T' + hours + ':' + minutes + ':' + seconds + 'Z';
  },

  getShortDate: function () {
    var d = new Date(),
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        // date = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate(),
        month = d.getMonth();

    return d.getDate() + ' ' + months[month] + ' ' + d.getFullYear();
  },

  sleep: function(s) {
    
    var e = new Date().getTime() + (s * 1000);

    while (new Date().getTime() <= e) {
      ;
    }
  },

  convertToISO8601: function (strDate) {
    // "2015-11-03T00:00:00Z"
    var d = new Date(strDate),
        date = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate(),
        month = d.getMonth() + 1;

    month = (month < 10) ? '0' + month : month;

    return d.getFullYear() + '-' + month + '-' + date + 'T00:00:00Z';
  },

  getYear: function (strDate) {
    var d = new Date(strDate);
    return d.getFullYear();
  }

};