fs = require 'fs'
path = require 'path'
csv = require 'csv'
trim = require 'trim'
_ = require 'underscore'
normalizer = require './libs/normalizer'
utils = require './libs/utils'
request = require 'request'
crypto = require 'crypto'

count = 0
noOfItems = 0
rawData = []
data = []
errors = []

cacheExists = fs.existsSync 'google-coord-cache.json'

if (cacheExists)
	cacheFile = fs.readFileSync 'google-coord-cache.json', { encoding: 'utf8' }
	coordCache = JSON.parse cacheFile
else
	coordCache = {}

# this is basically MacRitchie reservoir..
centre =
	lat: 1.340783
	lng: 103.818734

parser = csv.parse (err, data) ->

	count = 0
	noOfItems = data.length
	# noOfItems = 20
	rawData = data
	data = []

	# console.log '>> next item:', count
	parseRow rawData[count]

parseRow = (item) ->

	asc = {}

	asc.id = count + 1
	asc.dbapp_s = 'asc'

	asc.uen_s = item[0]

	# company..
	company = normalizer.titleCase item[1]
	company = normalizer.normalizePteLtd company
	asc.title_t = company

	# address..
	address = ''
	address = item[2] if item[2] isnt ''

	address += ' ' if address isnt ''
	address += normalizer.titleCase(item[3])

	# building name..
	address += ', ' + normalizer.titleCase(item[4]) if item[4] isnt ''

	# floor and unit..
	address += ' #' + item[5] if item[5] isnt ''
	address += '-' + item[6] if item[5] isnt '' and item[6] isnt ''

	# postal..
	postal = item[7] if item[7] isnt ''
	postal = '0' + postal if (postal? and postal.length < 6) # for postal codes that have their leading 0 removed from the excel sheet..

	asc.address_s = trim(address + ', Singapore ' + postal) if address isnt ''

	if (item[8] isnt '')
		unstructuredAddress = removeGarbage item[8]
		asc.unstructuredAddress_t = normalizer.titleCase unstructuredAddress

	asc.postal_s = postal if postal?

	# emails (multiple)..
	asc.emails_ss = createArray(item[9].toLowerCase()) if item[9] isnt ''

	# contact number..
	asc.contacts_ss = createArray(item[10]) if item[10] isnt ''

	# subscope..
	asc.subscope_ss = createArray(item[11]) if item[11] isnt ''

	asc.subscopeDescription_ss = createSubscopeDescriptionArray(item[12]) if item[12] isnt ''

	# # googleAddress = 'Singapore ' + postal
	googleAddress = address + ', Singapore ' + postal

	# asc.hash = crypto.createHash('md5').update(asc.uen).digest('hex')

	###
	# skipping geo-location..
	###
	data.push asc
	getNextItem()

	###
	# adding geo-location..
	###
	# if (postal?)
	# 	if (_.has(coordCache, postal))
	# 		# console.log '>> getting coords from cache..'

	# 		googleData = getCacheData postal
	# 		asc = _.assign asc, googleData

	# 		data.push asc
	# 		getNextItem()
	# 	else
	# 		console.log '!! waiting 1 sec to not break the query limit..'
	# 		utils.sleep 1
	# 		console.log '>> fetching coords from google..'
	# 		request 'https://maps.googleapis.com/maps/api/geocode/json?address=' + googleAddress, (error, response, body) ->
	# 			if (!error and response.statusCode is 200)

	# 				res = JSON.parse body

	# 				# save cache version..
	# 				coordCache[postal] = res

	# 				googleData = getCacheData postal
	# 				asc = _.assign asc, googleData

	# 				data.push asc
	# 				getNextItem()
	# 			else
	# 				console.log '** error fetching coords from google! **', postal
	# else
	# 	data.push asc
	# 	getNextItem()

createArray = (str) ->
	items = str.split /\s*,\s*/
	return items

createSubscopeDescriptionArray = (str) ->
	items = str.split /\s*,\s*/
	if (items[0].toLowerCase().indexOf('erection') > -1 and items[1].toLowerCase().indexOf('alteration') > -1)
		fullName = items.splice(0, 2).join(', ')
		items.unshift fullName
	return items

getNextItem = ->
	if (count < noOfItems - 1)
		count++
		parseRow rawData[count]
	else
		dedupeData()
		# console.log data
		# console.log 'done!'

dedupeData = ->
	cleanData = []

	# data = _.sortBy data, 'hash'
	data = _.groupBy data, 'uen_s'
	_.each data, (item) ->
		# console.log item.length
		if (item.length is 1)
			cleanData.push item[0]
		else
			console.log '** DUPLICATE FOUND **'
			provider = item[0]
			# i = 1
			# while i < item.length
			#   provider.email_ss = _.union provider.email_ss, item[i].email_ss
			#   provider.course_ss = _.union provider.course_ss, item[i].course_ss
			#   provider.contactNo_ss = _.union provider.contactNo_ss, item[i].contactNo_ss
			#   i++
			cleanData.push provider

	writeData cleanData

writeData = (cleanData) ->

	# add last modified date..
	obj =
		id: '__last_modified'
		lastupdated_s: utils.getShortDate()

	cleanData.push obj

	console.log '>> updating cache file..'
	fs.writeFileSync 'google-coord-cache.json', JSON.stringify(coordCache), { ecoding: 'utf8' }

	if (errors.length > 0)
		console.log '>> writing errors file..'
		fs.writeFileSync 'errors.json', JSON.stringify(errors), { ecoding: 'utf8' }

	console.log '>> saving data..'
	fs.writeFileSync 'output/asc.json', JSON.stringify(cleanData), { ecoding: 'utf8' }

	console.log '** complete! **'

getCacheData = (postal) ->
	googleCacheData = coordCache[postal]

	if (googleCacheData.status is 'ZERO_RESULTS') then console.log '>> ZERO_RESULTS for', postal

	if (!googleCacheData.results[0]?)
		errors.push postal
		return {}

	gIndex = 0

	if (googleCacheData.results.length > 1)
		len = googleCacheData.results.length
		i = 0
		regEx = new RegExp /singapore/i
		while i < len
			loc = googleCacheData.results[i]
			if (regEx.test(loc.formatted_address))
				gIndex = i
				break
			i++

	lat = parseFloat googleCacheData.results[gIndex].geometry.location.lat
	lng = parseFloat googleCacheData.results[gIndex].geometry.location.lng

	addressComponents = googleCacheData.results[gIndex].address_components

	country = _.find addressComponents, (item) ->
		if (_.contains(item.types, 'country'))
			return item

	neighbourhood = _.find addressComponents, (item) ->
		if (_.contains(item.types, 'neighborhood'))
			return item

	if (lat > centre.lat and lng < centre.lng)
		region = 'Northwest'
	else if (lat > centre.lat and lng > centre.lng)
		region = 'Northeast'
	else if (lat < centre.lat and lng < centre.lng)
		region = 'Southwest'
	else if (lat < centre.lat and lng > centre.lng)
		region = 'Southeast'

	obj =
		placeId_s: googleCacheData.results[gIndex].place_id
		location: lat + ',' + lng
		region_s: region

	obj.neighbourhood_s = neighbourhood.long_name if neighbourhood
	obj.country_s = country.long_name if country

	# return obj
	return if country.long_name is 'Singapore' then obj else null

removeGarbage = (str) ->
	console.log '1:', str
	if str is undefined then return
	cleaned = str.replace /\\r/g, " "
	cleaned = cleaned.replace /\s+/g, " "
	console.log '2:', cleaned
	# return _.trim(cleaned)
	return trim(cleaned)

normalizer.loadTerms './protwords-asc.txt', ->
	fs.createReadStream('csv/asc2.csv').pipe parser