fs = require 'fs'
path = require 'path'
csv = require 'csv'
trim = require 'trim'
_ = require 'lodash'
normalizer = require './libs/normalizer'
request = require 'request'
crypto = require 'crypto'
nimble = require 'nimble'
expander = require './libs/acronym-expander'
utils = require './libs/utils'
converter = require 'json-2-csv'

# if (process.env.COUNTRY is undefined)
#   console.log ''
#   process.exit 1

count = 0
noOfItems = 0
rawData = []
data = []
dups = []
deepDups = []

parser = csv.parse (err, dataRaw) ->

	if err then throw err

	count = 0
	noOfItems = dataRaw.length
	# noOfItems = 20
	rawData = dataRaw
	# data = []

	# console.log '>> next item:', count
	# parseRow rawData[count]

	_.each rawData, (item) ->
		parseRow item
	# dedupeData()
	deepClean data

parseRow = (item) ->

	occ = {}

	occ.id = ++count
	occ.dbapp_s = 'occ'

	# title..
	title = _.trim(normalizer.titleCase(item[0])) if item[0] isnt ''
	occ.title_t = title

	occ.ssoc_s = item[1] if item[1] isnt ''

	data.push occ
	# getNextItem()

###
getNextItem = ->
	# console.log count, noOfItems
	if (count < noOfItems - 1)
		count++
		console.log '>> next item:', count
		parseRow rawData.pop()
	else
		# dedupeData()
		writeData data
###

dedupeData = ->
	console.log '>> starting dedupe on data..'
	cleanData = []

	data = _.groupBy data, 'ssoc_s'
	_.each data, (item) ->
		if (item.length > 1)
			console.log 'dup -->', item
			dups.push item
		cleanData.push item[0]

	# writeData cleanData
	deepClean cleanData

deepClean = (data) ->
	console.log '>> starting deep clean on data..'
	cleanData = []
	while (data.length > 0)
		# get title..
		title = data.pop()
		# console.log '>> deep clean:', title.title_t
		titleTokens = getTokens title
		
		match = false
		matchId = null

		i = data.length
		while i--
			item = data[i]
			# console.log item
			itemTokens = getTokens item
			if (titleTokens.length is itemTokens.length)
				titleTokens.sort()
				itemTokens.sort()
				# console.log 'ssoc:', title.ssoc_s, item.ssoc_s
				if (_.isEqual(titleTokens, itemTokens) and (title.ssoc_s is item.ssoc_s))
					match = true
					matchId = i
					break
		if (match)
			# console.log 'deep dup -->', title, item, matchId
			console.log 'deep dup -->', title, item
			# deepDups.push [title, item]
			if (title.title_t.indexOf(',') > 0)
				cleanData.push item
				deepDups.push [title, item]
			else
				cleanData.push title
				deepDups.push [item, title]
			data.splice matchId, 1
		else
			# console.log '** no match:', title
			cleanData.push title
	console.log '------------------------------------------'
	# console.log cleanData
	# console.log deepDups
	writeDuplications()
	writeDeepDuplications()
	writeData cleanData
	writeAllOccupations cleanData
	# createExcel cleanData

writeAllOccupations = (cleanData) ->
	str = ''
	count = 0
	cleanData = _.sortBy(cleanData, ['title_t']);
	filename = 'all-occupations.txt'

	_.each cleanData, (item) ->
		count++
		str += item.title_t + "|" + item.ssoc_s + "\r"

	console.log '> writeAllOccupations --> count:', count
	console.log '> writeAllOccupations --> saving data to:', filename
	fs.writeFileSync './' + filename, str, { ecoding: 'utf8' }

getTokens = (title) ->
	words = title.title_t
	words = words.replace /,/, ""
	words = words.split ' '
	return words

removeGarbage = (str) ->
	if str is undefined then return
	cleaned = str.replace /Ê|�/gi, " "
	cleaned = cleaned.replace /\\r/, ""
	cleaned = cleaned.replace /\s+/g, " "
	cleaned = cleaned.replace /<\/strong>.*/g, ""
	cleaned = cleaned.replace /Website:.*/gi, ""
	return _.trim(cleaned)

writeData = (cleanData) ->
	
	obj =
		id: '__last_modified'
		lastupdated_s: utils.getShortDate()

	cleanData.push obj

	filename = 'occupations.json'
	console.log '>> saving data to:', filename
	fs.writeFileSync 'output/' + filename, JSON.stringify(cleanData), { ecoding: 'utf-8' }

	console.log '** complete! ** num docs:', cleanData.length

# not used anymore.. use writeDeepDuplications instead..
createExcel = (cleanData) ->
	str = ''
	count = 0
	filename = 'occupations-excel.txt'
	_.each cleanData, (item) ->
		count++
		str += item.title_t + "|" + item.ssoc_s + "\r"
	console.log '>> count:', count
	console.log '> saving data to:', filename
	fs.writeFileSync './' + filename, str, { ecoding: 'utf8' }

writeDeepDuplications = ->
	console.log '>> writing deep duplications.. number of duplicates:', deepDups.length
	str = ''
	_.each deepDups, (item) ->
		str += item[0].title_t + "|" + item[0].ssoc_s + "|" + item[1].title_t +  "|" + item[1].ssoc_s + "\r"
	filename = 'deep-duplications.txt'
	console.log '> saving data to:', filename
	fs.writeFileSync './' + filename, str, { ecoding: 'utf-8' }
	console.log '> deep duplication file made.'

writeDuplications = ->
	console.log '>> writing duplications.. number of duplicates:', dups.length
	str = ''
	_.each dups, (item) ->
		_.each item, (el) ->
			str += el.title_t + "|" + el.ssoc_s + "\r"
	filename = 'duplications.txt'
	console.log '> saving data to:', filename
	fs.writeFileSync './' + filename, str, { ecoding: 'utf-8' }
	console.log '> duplication file made.'

nimble.parallel [
	(cb) ->
		normalizer.loadTerms './protwords-occupations.txt', ->
			cb()
], ->
	# console.log 'done..', expander.terms
	fs.createReadStream('csv/occupations-update-160309.csv').pipe parser
	# fs.createReadStream('csv/occupations-sample.csv').pipe parser

# normalizer.loadTerms './protwords.txt', ->
# 	fs.createReadStream('csv/occ-sample-raw-data.csv').pipe parser