fs = require 'fs'
path = require 'path'
csv = require 'csv'
trim = require 'trim'
_ = require 'lodash'
normalizer = require './libs/normalizer'
request = require 'request'
crypto = require 'crypto'
nimble = require 'nimble'
expander = require './libs/acronym-expander'
utils = require './libs/utils'
converter = require 'json-2-csv'

# if (process.env.COUNTRY is undefined)
#   console.log ''
#   process.exit 1

count = 0
noOfItems = 0
rawData = []
data = []
errors = []

cacheExists = fs.existsSync 'google-coord-cache.json'

if (cacheExists)
	cacheFile = fs.readFileSync 'google-coord-cache.json', { encoding: 'utf-8' }
	coordCache = JSON.parse cacheFile
else
	coordCache = {}

# this is basically MacRitchie reservoir..
centre =
	lat: 1.340783
	lng: 103.818734

parser = csv.parse (err, data) ->

	count = 0
	noOfItems = data.length
	# noOfItems = 20
	rawData = data
	data = []

	# console.log '>> next item:', count
	parseRow rawData[count]

parseRow = (item) ->

	atp = {}

	atp.id = count + 1
	atp.dbapp_s = 'atp'

	# company..
	company = removeGarbage item[3]
	# if company is '' then console.log item, atp.id

	# contact number..
	contactNo = removeGarbage(item[4]) if item[4] isnt ''

	# competency subscope..
	course = removeGarbage(item[2]) if item[2] isnt ''

	# website..
	w = removeGarbage(item[5]) if item[5] isnt ''
	if (w? and w isnt '')
		if w.match /http/
			website = w
		else
			website = 'http://' + w

	# role..
	role = removeGarbage(item[6]) if item[6] isnt ''

	# status
	status = removeGarbage(item[7]) if item[7] isnt ''

	accreditationType = removeGarbage(item[8]) if item[8] isnt ''

	atp.title_t = company
	if contactNo isnt undefined then atp.contactNo_ss = [contactNo]
	if course isnt undefined then atp.course_ss = [course]
	if website isnt undefined then atp.website_s = website
	if role isnt undefined then atp.role_s = [role]
	if status isnt undefined then atp.status_s = [status]
	if accreditationType isnt undefined then atp.accreditationType_s = [accreditationType]
	# atp.hash = crypto.createHash('md5').update(company + role + status + accreditationType).digest('hex')
	atp.hash = crypto.createHash('md5').update(company).digest('hex')

	if company isnt '' then data.push atp
	getNextItem()

	# if (_.has(coordCache, postal))
	# 	# console.log '>> getting coords from cache..'

	# 	googleData = getCacheData postal
	# 	atp = _.assign atp, googleData

	# 	data.push atp
	# 	getNextItem()
	# else
	# 	console.log '!! waiting two seconds to not break the query limit..'
	# 	sleep.sleep 2
	# 	console.log '>> fetching coords from google..'
	# 	request 'https://maps.googleapis.com/maps/api/geocode/json?address=' + googleAddress, (error, response, body) ->
	# 		if (!error and response.statusCode is 200)

	# 			res = JSON.parse body

	# 			# save cache version..
	# 			coordCache[postal] = res

	# 			googleData = getCacheData postal
	# 			atp = _.assign atp, googleData

	# 			data.push atp
	# 			getNextItem()
	# 		else
	# 			console.log '** error fetching coords from google! **', postal

getNextItem = ->
	if (count < noOfItems - 1)
		count++
		# console.log '>> next item:', count
		parseRow rawData[count]
	else
		dedupeData()

dedupeData = ->
	cleanData = []

	# data = _.sortBy data, 'hash'
	data = _.groupBy data, 'hash'
	_.each data, (item) ->
		# console.log item.length
		if (item.length is 1)
			cleanData.push item[0]
		else
			provider = item[0]
			i = 1
			while i < item.length
				# provider.email_ss = _.union provider.email_ss, item[i].email_ss
				provider.course_ss = _.union provider.course_ss, item[i].course_ss
				# provider.fullCourse_ss = _.union provider.fullCourse_ss, item[i].fullCourse_ss
				# provider.courseFacet_ss = _.union provider.courseFacet_ss, item[i].courseFacet_ss
				provider.contactNo_ss = _.union(provider.contactNo_ss, item[i].contactNo_ss) # if item[i].contactNo_ss[0] isnt null
				# provider.role_ss = _.union(provider.role_ss, item[i].role_ss)
				i++
			if provider.contactNo_ss.length is 0 then delete provider.contactNo_ss

			cleanData.push provider

	writeData cleanData

removeGarbage = (str) ->
	if str is undefined then return
	cleaned = str.replace /Ê|�/gi, " "
	cleaned = cleaned.replace /\\r/, ""
	cleaned = cleaned.replace /\s+/g, " "
	cleaned = cleaned.replace /<\/strong>.*/g, ""
	cleaned = cleaned.replace /Website:.*/gi, ""
	return _.trim(cleaned)

writeData = (cleanData) ->
	# add last modified date..
	obj =
		id: '__last_modified'
		lastupdated_s: utils.getShortDate()

	cleanData.push obj
	
	# console.log '>> updating cache file..'
	# fs.writeFileSync 'google-coord-cache.json', JSON.stringify(coordCache), { ecoding: 'utf-8' }

	# if (errors.length > 0)
	# 	console.log '>> writing errors file..'
	# 	fs.writeFileSync 'errors.json', JSON.stringify(errors), { ecoding: 'utf-8' }

	# converter.json2csv cleanData, (err, csv) ->
	# 	if err then throw err
	# 	console.log csv

	console.log '>> saving data..'
	fs.writeFileSync 'output/atp-data.json', JSON.stringify(cleanData), { ecoding: 'utf-8' }

	console.log '** complete! ** num docs:', cleanData.length

getCacheData = (postal) ->
	googleCacheData = coordCache[postal]

	if (!googleCacheData.results[0]?)
		errors.push postal
		return {}

	gIndex = 0

	if (googleCacheData.results.length > 1)
		len = googleCacheData.results.length
		i = 0
		regEx = new RegExp /singapore/i
		while i < len
			loc = googleCacheData.results[i]
			if (regEx.test(loc.formatted_address))
				gIndex = i
				break
			i++

	lat = parseFloat googleCacheData.results[gIndex].geometry.location.lat
	lng = parseFloat googleCacheData.results[gIndex].geometry.location.lng

	addressComponents = googleCacheData.results[gIndex].address_components

	country = _.find addressComponents, (item) ->
		if (_.contains(item.types, 'country'))
			return item

	neighbourhood = _.find addressComponents, (item) ->
		if (_.contains(item.types, 'neighborhood'))
			return item

	if (lat > centre.lat and lng < centre.lng)
		region = 'Northwest'
	else if (lat > centre.lat and lng > centre.lng)
		region = 'Northeast'
	else if (lat < centre.lat and lng < centre.lng)
		region = 'Southwest'
	else if (lat < centre.lat and lng > centre.lng)
		region = 'Southeast'

	obj =
		placeId_s: googleCacheData.results[gIndex].place_id
		location: lat + ',' + lng
		region_s: region

	obj.neighbourhood_s = neighbourhood.long_name if neighbourhood
	obj.country_s = country.long_name if country

	return obj

nimble.parallel [
	(cb) ->
		normalizer.loadTerms './protwords.txt', ->
			cb()
	(cb) ->
		expander.loadTerms './terms.csv', ->
			cb()
], ->
	# console.log 'done..', expander.terms
	fs.createReadStream('csv/training-providers-final.csv').pipe parser

# normalizer.loadTerms './protwords.txt', ->
# 	fs.createReadStream('csv/atp-sample-raw-data.csv').pipe parser